from setuptools import setup

setup(
    name='ez-scrip-lib',
    version='0.1.0',
    description='lots of functions to help you script',
    url='https://github.com/codyc4321/webdriver_chauffeur',
    author='Cody Childers',
    author_email='cchilder@mail.usf.edu',
    license='MIT',
    packages=['webdriver_chauffeur'],
    install_requires=[
        'bs4',
        'selenium'
    ],
    zip_safe=False,
)

import datetime

def get_timestamp(time_format='%m-%d-%Y_%I:%M:%S%p'):
    return datetime.datetime.now().strftime(time_format)
     
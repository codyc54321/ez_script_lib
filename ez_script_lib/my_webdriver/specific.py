#!/usr/bin/env python

import datetime, time, subprocess, re

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select as RegularSelect
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

from ez_scrip_lib.config import *
from ez_scrip_lib.my_webdriver.general import *


"""
WedDriverWait: http://stackoverflow.com/questions/26267565/webdriverwait-failing-even-though-element-is-present
"""

#-------------------------------------------------------------------------------------------------------------

"""bitbucket"""

BITBUCKET_URL = "https://bitbucket.org/"

HOSTNAMES = {
    'codyc54321': 'bitbucket.org',
    'codyc54321_old': 'old',
    'codyc54321_tutorial': 'tutorial',
}


class BitbucketDriver(FirefoxDriver):

    def __init__(self, username="codyc54321", password=OOPS_PW):
        self.username = username
        self.password = password
        self.main_url  = "https://bitbucket.org/"
        self.start_url="https://bitbucket.org/account/signin/"
        self.driver = FirefoxDriver()

    def login(self):
        """http://stackoverflow.com/questions/27927964/selenium-element-not-visible-exception"""
        self.driver.get(self.start_url)
        time.sleep(2)
        self.find_box_and_fill(search_text="username", value=self.username)
        self.find_box_and_fill(search_text="password", value=self.password)
        self.click_button("Log in")
        # element = self.locate_element("password")
        # element.send_keys(Keys.TAB)
        # self.active_element.send_keys(Keys.ENTER)
        time.sleep(2)

    def create_repo(self, repo_name=None, description=None):
        self.login()
        self.access_link(search_text='create-repo-link')
        self.find_box_and_fill(search_text='id_name', value=repo_name)
        if description:
            self.find_box_and_fill(search_text='id_description', value=description)
        self.submit_form(search_text='Create repository')
        time.sleep(1)
        self.quit()

    def delete_repo(self, repo_name=None):
        self.login()
        self.get(BITBUCKET_URL + '{username}/{repo}/delete'.format(username=self.username, repo=repo_name))
        time.sleep(1)
        self.submit_form(search_text='Delete repository')
        time.sleep(1)
        # subprocess.call('xdotool key Return', shell=True)
        self.click_active_by_hitting_enter()
        time.sleep(12)
        self.quit()


class StashDriver(object):

    BASE_URL = "https://the_url"
    START_URL = BASE_URL + "/login"
    PULL_REQUESTS_BASE_URL = BASE_URL + "projects/ITSME/repos/{}/pull-requests"

    def __init__(self, username="cchilders", password=THE_USUAL, name="Cody Childers"):
        self.username = username
        self.password = password
        self.name = name
        self.driver = FirefoxDriver()

    def login(self):
        self.driver.get(self.START_URL)
        self.driver.find_box_and_fill(search_text="j_username", value=self.username)
        self.driver.find_box_and_fill(search_text="j_password", value=self.password)
        remember_me_checkbox = self.driver.find_element_by_id('_spring_security_remember_me')
        remember_me_checkbox.click()
        self.driver.click_button(search_text="submit")

    def add_ssh_key(self, ssh_key):
        self.access_link_by_text("Manage account")
        self.access_link_by_text("SSH keys")
        self.access_link_by_text("Add key")
        time.sleep(2)

        self.find_box_and_fill(html_id="text", value=ssh_key)
        self.submit_form(html_id="submit")

    def gather_pull_requests(self, repo):
        self.driver.get(self.PULL_REQUESTS_BASE_URL.format(repo))
        soup = self.driver.soup
        divs = soup.find_all('div', attrs={'title': self.name})
        prs = []
        for div in divs:
            prs.append(self.get_pull_request_info(div))
        return prs

    def get_pull_request_info(self, tag):
        """takes divs with title="Cody Childers" """
        data = tag.parent.previous_sibling
        attribute_tag = data.find('a')
        relative_url = attribute_tag.attrs['href']
        pr_title = attribute_tag.attrs['title']
        return {'title': pr_title, 'relative_url': relative_url}

    def gather_pull_request_comments(self, relative_url):
        url = self.BASE_URL + relative_url
        self.driver.get(url)

    def has_conflicts(self, text):
        return "This pull request can't be merged" in text

#----------------------------------------------------------------------------------------------------

class HCCDriver(object):

    """
    open new tab off link
    https://gist.github.com/lrhache/7686903

    open new tab
    http://stackoverflow.com/questions/17547473/how-to-open-a-new-tab-using-selenium-webdriver
    """
    def __init__(self):
        self.email = HCC_EMAIL
        self.email_login_password = APE_NINE


    def main_page(self):
        self.get('https://www.hccfl.edu/hawknet.aspx')

    def login_webadvisor(self):
        webadvisor_url = "https://hccadvisor.hccfl.edu"
        self.webadvisor_driver = FirefoxDriver()
        self.webadvisor_driver.get(webadvisor_url)
        time.sleep(2)
        self.webadvisor_driver.access_link(search_text="Log In")
        self.webadvisor_driver.find_box_and_fill(search_text="LABELID_USER_NAME", value="cchilders")
        self.webadvisor_driver.find_box_and_fill(search_text="CURR.PWD", value=HCC_WEBADVISOR_PW)
        self.webadvisor_driver.submit_form(search_text="SUBMIT")
        self.webadvisor_driver.access_link(search_text="Students")

    def login_email(self):
        self.email_driver = FirefoxDriver()
        self.email_driver.get("http://outlook.com/hawkmail.hccfl.edu")
        # WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(By.ID, 'ctl00_ContentPlaceHolder1_UsernameTextBox'))
        self.email_driver.find_box_and_fill(search_text="ctl00_ContentPlaceHolder1_UsernameTextBox", value=self.email)
        self.email_driver.find_box_and_fill(search_text="ctl00_ContentPlaceHolder1_PasswordTextBox", value=self.email_login_password)
        time.sleep(2)
        self.email_driver.submit_form("ctl00_ContentPlaceHolder1_SubmitButton")

    def login_myhcc(self):
        self.myhcc_driver = FirefoxDriver()
        self.myhcc_driver.get("https://hcc.instructure.com")
        time.sleep(1)
        self.myhcc_driver.find_box_and_fill(search_text='ctl00_ContentPlaceHolder1_UsernameTextBox', value='cchilders@hawkmail.hccfl.edu')
        # self.myhcc_driver.find_box_and_fill(search_text='ctl00_ContentPlaceHolder1_PasswordTextBox', value=APE_NINE_SLASH)

    def login_testout(self):
        URL = "http://cdn.testout.com/client-v5-1-10-293/startlabsim.html?culture=en-us"
        self.testout_driver = FirefoxDriver()
        # driver.get("http://www.testout.com/")
        self.testout_driver.get(URL)
        self.testout_driver.find_box_and_fill('tbLogin', 'cchilders')
        self.testout_driver.find_box_and_fill('tbPassword', THE_USUAL)
        self.testout_driver.click_button("bBorder.Border")

    def access_class_search_screen_hcc(self, category="CTS", driver=None):
        search_classes_url = "http://www.hccfl.edu/searchsections/index.asp"
        self.get(search_classes_url)
        self.pick_select_box(search_text="Subject", value=category.upper())
        self.find_box_and_fill(search_text="Start", value=get_datestring_for_today(),  driver=driver)
        self.click_button("Submit", driver)

    def get_datestring_for_today(date_format='%m/%d/%Y'):
        today = datetime.date.today()
        return today.strftime(date_format)

    def send_transcript(self, school_obj):
        self.transcript_driver = FirefoxDriver()

        self.transcript_driver.get("http://www.hccfl.edu/hawknet.aspx")
        time.sleep(3)

        self.transcript_driver.get("https://hccadvisor.hccfl.edu/")
        time.sleep(3)

        self.transcript_driver.access_link(search_text="Log In")

        self.transcript_driver.find_box_and_fill(search_text="USER_NAME", value="cchilders")
        self.transcript_driver.find_box_and_fill(search_text="CURR_PWD", value="0535651")
        self.transcript_driver.submit_form(search_text="SUBMIT")

        self.transcript_driver.access_link(search_text="Students")

        self.transcript_driver.access_link(search_text="Transcript Request")

        self.transcript_driver.find_box_and_fill(search_text="VAR1", value=school_obj.recipient)
        self.transcript_driver.find_box_and_fill(search_text="VAR.ADDRESS.LINES_1", value=school_obj.address1)
        if school_obj.address2:
            self.transcript_driver.find_box_and_fill(search_text="VAR.ADDRESS.LINES_2", value=school_obj.address2)

        broken_state_selectbox = self.transcript_driver.locate_element(search_text="VAR_STATE")
        broken_state_selectbox.send_keys('t')
        broken_state_selectbox.send_keys('t')

        self.transcript_driver.find_box_and_fill(search_text="VAR.CITY", value=school_obj.city)
        self.transcript_driver.find_box_and_fill(search_text="VAR.ZIP", value=school_obj.zipcode)

        # broken????
        # self.transcript_driver.pick_select_box(search_text="VAR3", value="1")
        time.sleep(10)


class USFDriver(object):

    def send_transcript(self, school_obj, card):
        self.transcript_driver = FirefoxDriver()

        #main page with login
        self.transcript_driver.get("https://my.usf.edu")
        self.transcript_driver.find_box_and_fill(search_text="username", value="cchilder")
        self.transcript_driver.find_box_and_fill(search_text="password", value=APE_NINE)
        self.transcript_driver.submit_form(search_text="fm1")

        # pick Oasis link, need sleep as it was lagging and wouldn't wait to load
        # alternatives to time.sleep() if needed:
        # http://stackoverflow.com/questions/26566799/selenium-python-how-to-wait-until-the-page-is-loaded
        self.transcript_driver.get("https://usfonline.admin.usf.edu")
        time.sleep(1)
        self.transcript_driver.access_link(search_text="Student")
        time.sleep(1)
        self.transcript_driver.access_link(search_text="Student Records")
        time.sleep(1)
        self.transcript_driver.access_link(search_text="Request an Official Transcript")

        self.transcript_driver.find_box_and_fill(search_text='issue_to_in', value=school_obj.recipient)

        self.transcript_driver.submit_form(search_text="Continue")
        time.sleep(1)

        #address form
        self.transcript_driver.find_box_and_fill(search_text='street1_in', value=school_obj.address1)
        if school_obj.address2:
            self.transcript_driver.find_box_and_fill(search_text='street2_in', value=school_obj.address2)
        self.transcript_driver.find_box_and_fill(search_text='city_in', value=school_obj.city)

        self.transcript_driver.pick_select_box(search_text="state_in", value=school_obj.state)
        self.transcript_driver.find_box_and_fill(search_text="zip_in", value=school_obj.zipcode)

        self.transcript_driver.submit_form(search_text="Continue")
        time.sleep(1)

        self.transcript_driver.submit_form(search_text="Continue")

        self.transcript_driver.submit_form(search_text="Submit Request")
        time.sleep(3)

        self.transcript_driver.pick_select_box(search_text="pmtMethod", value="new_cc")
        time.sleep(1.5)

        self.transcript_driver.find_box_and_fill(search_text='cardNum', value=card.card_number)
        time.sleep(1.5)

        self.transcript_driver.submit_form(search_text="Continue")
        self.transcript_driver.find_box_and_fill(search_text="NAME_ON_CARD", value=card.name_on_card)
        self.transcript_driver.pick_select_box(search_text="EXP_MO", value=card.expiration_month)
        self.transcript_driver.pick_expiration_year_for_card(expiration_year=card.expiration_year, search_text="EXP_YR")

        self.transcript_driver.find_box_and_fill(search_text="cvv", value=card.security_code)
        self.transcript_driver.find_box_and_fill(search_text="address1", value=card.address1)
        self.transcript_driver.find_box_and_fill(search_text="city1", value=card.city)
        self.transcript_driver.pick_select_box(search_text="state1", value=card.state)
        self.transcript_driver.find_box_and_fill(search_text="zipCode", value=card.zipcode)
        self.transcript_driver.pick_select_box(search_text="country", value='US')
        self.transcript_driver.find_box_and_fill(search_text="email", value=EMAIL)


class FSUDriver(object):
    def login(self):
        fsu_driver = FirefoxDriver()

        fsu_driver.get("https://my.fsu.edu")
        time.sleep(0.3)

        fsu_driver.find_box_and_fill("username", value="ckc05")
        fsu_driver.find_box_and_fill("password", value=THE_USUAL)

        time.sleep(5)

        fsu_driver.get("https://cas.fsu.edu/cas/?service=https://campus.omni.fsu.edu/psp/sprdcs/EMPLOYEE/HRMS/c/SA_LEARNER_SERVICES.SSS_TSRQST_OFF.GBL")


#----------------------------------------------------------------------------------------------------

class GREDriver(object):

    def __init__(self, username="0900887851", password=OOPS_PW,
            start_url="https://ondemand.ufcu.org/hbnet/login/login.aspx?organization=FB366962-6E2E-4c57-91D0-23F36957C653"):
        self.username  = username
        self.password  = password
        self.start_url = start_url

    """ works!"""
    def login(self):
        """gets you logged in, at account overview page"""
        self.driver = FirefoxDriver()
        self.driver.get(self.start_url)
        self.driver.driver.execute_script("document.getElementById('ctlUserName_txtNumber').setAttribute('value', '{}')".format(self.username))
        self.driver.driver.execute_script("document.getElementById('txtPassword').setAttribute('value', '{}')".format(self.password))
        self.driver.click_button('Login')


class YoutubeDriver(object):

    start_url = "https://youtube.com"

    def __init__(self, username="cchilder@mail.usf.edu", password=THE_USUAL):
        self.username  = username
        self.password  = password
        self.driver    = FirefoxDriver()

    def login(self):
        self.driver.get(self.start_url)

        sign_in_xpath = "//span[contains(text(), 'Sign in')]"
        span_element = self.driver.driver.find_element_by_xpath(sign_in_xpath)
        mouse = webdriver.ActionChains(self.driver.driver)
        #click sign in button to get to email page
        mouse.move_to_element(span_element).click().perform()

        # enter email and click next
        time.sleep(2)
        self.driver.active_element.send_keys('cchilder@mail.usf.edu')
        self.driver.click_button('next')

        xpath = "//p[contains(text(), 'Organizational')]"
        organizational_button = self.driver.driver.find_element_by_xpath(xpath)
        mouse = webdriver.ActionChains(self.driver.driver)
        mouse.move_to_element(organizational_button).click().perform()

        self.driver.active_element.send_keys('cchilder')
        self.driver.tab_through()
        self.driver.active_element.send_keys(YOUTUBE_PW)
        self.driver.tab_through()
        self.driver.active_element.click()


class FacebookDriver(object):
    start_url = "https://facebook.com"

    def __init__(self, username="ckc05@fsu.edu", password=OOPS_PW):
        self.username  = username
        self.password  = password
        self.driver    = FirefoxDriver()

    def login(self):
        self.driver.get(self.start_url)
        time.sleep(2)
        self.driver.find_box_and_fill('email', self.username)
        self.driver.find_box_and_fill('pass', self.password)
        self.driver.click_button('u_0_x')

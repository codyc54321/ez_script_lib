import re
import time

from ez_scrip_lib.config import *
from ez_scrip_lib.my_webdriver.general import FirefoxDriver


class BankDriver(object):

    def clean_dollars_to_float(self, dollars_string):
        clean_string = dollars_string.replace('$', '').replace(',', '').strip()
        return float(clean_string)


class USFCreditUnionDriver(BankDriver):

    def __init__(self, username=USFFCU_USERNAME, password=, start_url="https://usffcu.org/"):
        super(USFCreditUnionDriver, self).__init__()
        self.username  = username
        self.password  = password
        self.start_url = start_url

    """works"""
    def login(self):
        """gets you logged in, at account overview page"""
        self.driver = FirefoxDriver()
        self.driver.sleep_random(170, 250)
        self.driver.get(self.start_url)
        self.driver.find_box_and_fill(search_text="id", value=self.username)
        self.driver.submit_form(search_text="Submit")

        """now fill in password on the next page"""
        self.driver.wait("LoginPwdTextBox")
        import ipdb; ipdb.set_trace()
        self.driver.find_box_and_fill(search_text="ctl00_PageContent_Login1_PasswordTextBox", value=self.password)
        self.driver.click_button("ctl00_PageContent_Login1_PasswordSubmitButton")

    def get_checking_amount(self):
        if not self.logged_in:
            self.login()

        # try:
        #     WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'ctl00_ctl26_secondaryMenuContainer')))

        self.driver.wait('ctl00_ctl26_secondaryMenuContainer')
        soup = self.driver.soup
        checking_td = soup.find('td', text='Checking')
        new_line_sib = checking_td.nextSibling
        available_amount_td = new_line_sib.nextSibling
        available_personal_checking_amount = self.clean_dollars_to_float(available_amount_td.text)
        self.driver.close()
        return available_personal_checking_amount
        # except TimeoutException:
        #     print("Loading took too much time")

    @property
    def logged_in(self):
        try:
            return "https://cm.netteller.com/login2008/Views" in self.driver.current_url
        except:
            return False


class UFCUDriver(BankDriver):

    def __init__(self, username=UFCU_USERNAME, password=OOPS_PW,
            start_url="https://ondemand.ufcu.org/hbnet/login/login.aspx?organization=FB366962-6E2E-4c57-91D0-23F36957C653"):
        self.username  = username
        self.password  = password
        self.start_url = start_url

    """ works!"""
    def login(self):
        """gets you logged in, at account overview page"""
        self.driver = FirefoxDriver()
        time.sleep(1)
        self.driver.get(self.start_url)
        time.sleep(3)
        self.driver.execute_script("document.getElementById('ctlUserName_txtNumber').setAttribute('value', '{}')".format(self.username))
        self.driver.execute_script("document.getElementById('txtPassword').setAttribute('value', '{}')".format(self.password))
        self.driver.click_button('Login')

    """security questions cant be defeated because it generates a picture and the HTML doesnt show the answer!!!"""
    # def defeat_questions(self):
    #     soup = self.driver.soup


    def get_UFCU_total(self):

        def get_total():
            # time.sleep(5)
            self.driver.wait('TopMenuCtl_ddlAccounts')
            soup = self.driver.soup
            total_td = soup.find('td', text='Total')
            if not total_td:
                import ipdb; ipdb.set_trace()
            available_amount_td = total_td.nextSibling
            return self.clean_dollars_to_float(available_amount_td.text)

        if not self.logged_in:
            self.login()

        soup = self.driver.soup
        if 'security answer' in soup.text:
            while True:
                response = input("Answer the security question then press enter")
                if response == "":
                    break

        available_personal_checking_amount = get_total()

        # get credit card total
        soup = self.driver.soup
        visa_tag = soup.findAll('a', text = re.compile('.*Visa.*'))[0]
        visa_card_credits = self.clean_dollars_to_float(visa_tag.parent.nextSibling.text)

        # get hard money loan total
        soup = self.driver.soup
        hard_money_tag = soup.findAll('a', text='Cons-loc')[0]
        hard_money_credits = self.clean_dollars_to_float(hard_money_tag.parent.nextSibling.text)


        # get business checking
        self.driver.pick_select_box("TopMenuCtl$ddlAccounts", '0')
        soup = self.driver.soup
        available_rental_checking_amount = get_total()

        # get business checking
        self.driver.pick_select_box("TopMenuCtl$ddlAccounts", '1')
        soup = self.driver.soup
        available_bizness_checking_amount = get_total()

        total = (
            available_personal_checking_amount + available_rental_checking_amount
            + available_bizness_checking_amount - visa_card_credits - hard_money_credits
        )
        self.driver.close()
        return total

    @property
    def logged_in(self):
        try:
            soup = self.driver.soup
        except:
            return False


class BankingDriver(object):

    def __init__(self):
        self.ufcu_driver = UFCUDriver()
        self.usffcu_driver = USFCreditUnionDriver()

    def money_count(self):
        ufcu_cash = self.ufcu_driver.get_UFCU_total()
        usffcu_cash = self.usffcu_driver.get_checking_amount()
        total = ufcu_cash + usffcu_cash
        print(total)
        return total

    def quit(self):
        self.ufcu_driver.quit()
        self.usffcu_driver.quit()


def money_count():
    usffcu_driver = USFCreditUnionDriver()
    usffcu_cash = usffcu_driver.get_checking_amount()
    ufcu_driver = UFCUDriver()
    ufcu_cash = ufcu_driver.get_UFCU_total()
    total = ufcu_cash + usffcu_cash
    print(total)
    return total

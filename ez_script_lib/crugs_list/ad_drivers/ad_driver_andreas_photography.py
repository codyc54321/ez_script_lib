import os
import datetime
import time

from faker import Faker
FAKER = Faker()

from ez_scrip_lib.config import THE_USUAL
from ez_scrip_lib.crugs_list import CraigslistDriver


class BasicPostObject(object):
    def __init__(self):
        self.contact_name = ""
        self.contact_phone = "2104143931"
        self.city = "Austin"
        self.postal_code = "78701"


class AndreasPhotographyPostsDriver(CraigslistDriver):

    def __init__(self, username="cchilder@mail.usf.edu", password=THE_USUAL):
        super(AndreasPhotographyPostsDriver, self).__init__(username=username, password=password)

    def post_photography_ad(self, category='event_services', images=[], city='austin',
            available_on=datetime.date.today, **kwargs):
        # TODO: set city to 'austin' and substitute the city in the kwargs to be used in posting_obj
        self.city = city
        self.start_url = self.generate_start_url_by_city()

        # self.images = images

        kwargs.update({'city': city.capitalize()})
        posting_obj = self.generate_posting_obj(kwargs)

        # import ipdb; ipdb.set_trace()
        self.post_an_add(category, posting_obj)
        # self.fill_form_fields(self.get_room_rental_form_tasks(available_on))
        self.driver.click_button('go')
        # self.driver.wait(search_item="imgbox", search_type="class")
        time.sleep(5)
        self.driver.click_button('continue')
        self.driver.basic_sleep('short')
        self.add_images_and_move_on()
        self.driver.basic_sleep('long')
        element = self.driver.locate_element("publish")
        element.click()
        self.driver.basic_sleep('long')
        self.ensure_recent_ad_is_published()

    def generate_posting_obj(self, attrs):
        posting_obj = BasicPostObject()
        for key, value in attrs.items():
            if key == 'PostingTitle':
                setattr(posting_obj, 'PostingTitle', FAKER.random_element(value))
            else:
                setattr(posting_obj, key, value)
        print(posting_obj)
        print(posting_obj.PostingTitle)
        return posting_obj

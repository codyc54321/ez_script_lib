import os


house_pics = '{home}/my_documents/craigslist/house'.format(home=os.path.expanduser('~'))
upstairs_room_behind_desk_path = os.path.join(house_pics, 'upstairs_room_behind_desk')
general_pics = os.path.join(house_pics, 'general_pics')

upstairs_room_behind_desk_images = [
    os.path.join(general_pics, 'front.jpg'),
    os.path.join(general_pics, 'kitchen.jpg'),
    os.path.join(general_pics, 'main.jpg'),
    os.path.join(general_pics, 'second_floor.jpg'),
    os.path.join(upstairs_room_behind_desk_path, 'bed.jpg'),
    os.path.join(upstairs_room_behind_desk_path, 'closet.jpg'),
    os.path.join(upstairs_room_behind_desk_path, 'furniture.jpg'),
]

DEPOSIT = 400
ASKING_PRICE = 530
TOTAL_PRICE = DEPOSIT + ASKING_PRICE


regular_room_body =  """
Room available in large, furnished, home

Your room is furnished too

{asking_price}, minimum 3 months

Washer and dryer in the house

All utilities included, including wifi

Bathroom is shared. Maid comes twice a month

Rules are pretty basic:
- No loud music late at night, people go to bed about midnight
- Clean up kitchen after cooking/eating. That means immediately after eating, not several hours later or tomorrow.
- No littering around house

No animals please.

{deposit} refundable deposit

{total_price} total to move in

Please don't ask questions answered on the ad. I will answer anything you wanna know that isn't listed here

If interested, leave the following exact information to prove you read the ad and aren't spamming. I will call to set up showing

1. The date the room is available (it says so in the ad, or it says 'available now')
2. What the total move in price is
3. What general area it is in relation to downtown Austin (north, east, south, west. A map is provided right on this page)
4. Your number to call and when you can talk (days and/or hours of day)

All inquiries
without this information prove the person did not read the ad and they aren't seriously looking for a room, so will be deleted.
Serious inquiries only.
""".format(asking_price=ASKING_PRICE, deposit=DEPOSIT, total_price=TOTAL_PRICE)

livein_maid_body = """
Room available in large, furnished, home

Your room is furnished too

{asking_price}, minimum 3 months

Some rent can be worked off. I currently need:
  -trash taken out
  -laundry done weekly
  -floors cleaned weekly
  -meals made weekly
  -bathrooms cleaned biweekly (biweekly == twice per month)
  -carpet vacuumed/steam cleaned biweekly
  -grass mowed monthly

Work expected is 10 hours a week. No lazy people need inquire. Rent is paid upfront each month and refunded upon completion of duties.

Washer and dryer in the house

All utilities included, including wifi

Bathroom is shared

Rules are:
- No loud music late at night
- Clean up kitchen after cooking/eating. That means immediately after eating, not several hours later or tomorrow.

No animals please.

{deposit} refundable deposit

{total_price} total to move in

Please don't ask questions answered on the ad. I will answer anything you wanna know that isn't listed here

If interested, leave the following exact information to prove you read the ad and I call to set up showing:

1. The date the room is available (it says so in the ad)
2. What you can provide (examples: "I don't mind cleaning, but I'm also a hairdresser", etc)
3. What the total move in price is
4. Your number to call and when you can talk (days and/or hours of day)

All inquiries without this information prove the person did not read the ad and they aren't seriously looking for a room, so will be deleted.
Serious inquiries only.
""".format(asking_price=ASKING_PRICE, deposit=DEPOSIT, total_price=TOTAL_PRICE)

regular_room_titles = [
    "Room available in, large furnished home, at excellent price",
    "Quiet living in spacious house, perfect for grad student",
    "Rooms open East Austin, all bills paid and wifi ${asking_price}".format(asking_price=ASKING_PRICE),
]

livein_maid_titles = [
    "Room available in, large furnished home, work instead of rent",
    "Quiet living in spacious house; work instead of pay",
    "No rent- work instead, 10 hours/week; East Austin",
]

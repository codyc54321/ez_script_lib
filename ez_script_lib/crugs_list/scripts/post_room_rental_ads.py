#!/usr/bin/env python

import datetime, time, random

from ez_scrip_lib.crugs_list.ad_drivers.data_room_rentals import *
from ez_scrip_lib.crugs_list.ad_drivers.ad_driver_room_rentals import RoomRentalPostsDriver


data = {
    'regular': {
        'PostingTitle': regular_room_titles,
        'PostingBody': regular_room_body,
        'Ask': ASKING_PRICE,
        'available_on': datetime.date(2017, 6, 25),
        'images': upstairs_room_behind_desk_images
    },
    'live_in_help': {
        'PostingTitle': livein_maid_titles,
        'PostingBody': livein_maid_body,
        'Ask': ASKING_PRICE,
        'available_on': datetime.date(2017, 6, 25),
        'images': upstairs_room_behind_desk_images
    }
}


def run_room_posting(category, city="austin", data={}, driver=None):
    if not driver:
        driver = RoomRentalPostsDriver()
    driver.post_room_rental(
        category=category,
        city=city,
        **data
    )
    time.sleep(4)
    driver.driver.quit()

driver = RoomRentalPostsDriver()
# driver.delete_all_active_ads()
DELAY=150
TYPE = 'regular'

run_room_posting("rooms_and_shares", data=data[TYPE], driver=driver)
print('Sleeping for {} seconds...'.format(DELAY))
time.sleep(DELAY)
run_room_posting("sublets_and_temp", data=data[TYPE], driver=driver)

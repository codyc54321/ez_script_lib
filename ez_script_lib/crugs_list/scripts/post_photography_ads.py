#!/usr/bin/env python

import datetime, time, random

from ez_scrip_lib.crugs_list.ad_drivers.data_andreas_photography import *
from ez_scrip_lib.crugs_list.ad_drivers.ad_driver_andreas_photography import AndreasPhotographyPostsDriver


data = {
    'PostingTitle': titles,
    'PostingBody': body,
    # 'images': upstairs_room_behind_desk_images
}


def run_photography_posting(category, city="austin", data={}, driver=None):
    if not driver:
        driver = AndreasPhotographyPostsDriver()
    driver.post_photography_ad(
        category=category,
        city=city,
        **data
    )
    time.sleep(4)
    driver.driver.quit()

driver = AndreasPhotographyPostsDriver(username="urallbads20@mail.com")
# driver.delete_all_active_ads()
DELAY=150

run_photography_posting("creative_services", data=data, driver=driver)
print('Sleeping for {} seconds...'.format(DELAY))
time.sleep(DELAY)
run_photography_posting("event_services", data=data, driver=driver)

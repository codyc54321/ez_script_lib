import os, random, platform
from functools import wraps


class single_item_or_list(object):

   def __init__(self, func):
        self.func = func
        wraps(func)(self)

   def __call__(self, *args, **kwargs):
        if type(args[0]) == list:
            print('made it here')
            this_list = args[0]
            new_list = []
            for item in this_list:
                new_args_list = [item]
                for thing in args[1:]:
                    new_args_list.append(thing)
                new_args = tuple(new_args_list)
                new_list.append(self.func(*new_args))
            return new_list
        else:
            ret = self.func(*args, **kwargs)
            return ret


#----------------------------------------------------------------------------------------------------
# @linux_or_mac({'command': {'linux': 'sudo apt-get update', 'mac': 'brew update'})

class linux_or_mac(object):

    def __init__(self, arg_mapper):
        self.arg_mapper = arg_mapper

    def __call__(self, f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            kwargs.update(self.gather_kwargs())
            print('in the wrapper')
            print(kwargs)
            f(*args, **kwargs)
        return wrapped

    def gather_kwargs(self):
        return {key: self.arg_mapper[key][self.system] for key in self.arg_mapper.keys()}

    @property
    def system(self):
        system = platform.system()
        MAP = {'Darwin': 'mac', 'Linux': 'linux'}
        return MAP[system]

#----------------------------------------------------------------------------------------------------

class log_decorator(object):

   def __init__(self, func):
        self.func = func
        self.logpath = "~/logs"
        wraps(func)(self)

   def __call__(self, *args, **kwargs):
        this_path = os.path.join(self.logpath, self.func.__name__)
        ret = self.func(*args, **kwargs)
        open(this_path, 'w').close()

        if ret:
            with open(this_path, 'a') as myfile:
                myfile.write("Arguments were: {}, {}\n".format(args, kwargs))
                for line in ret:
                    l = str(line)
                    myfile.write(l)
                    myfile.write('\n')
        return ret

#----------------------------------------------------------------------------------------------------

class random_object_picker(object):
    """decorate with how many random objects to grab, and a queryset callback (which can just be the model name):

        @random_object_picker(50, Account)
        def print_id(*args, **kwargs):
            obj = kwargs['random_object']
            print(obj.pk)
    """

    def evaluate_callback(self, callback):
        try:
            return callback.objects.all()
        except:
            return callback

    def __init__(self, iterations, callback):
        self.iterations         = iterations
        self.queryset           = self.evaluate_callback(callback)
        self.queryset_length    = self.queryset.count()
        print((self.queryset_length))

    def get_random(self, upper_range):
        """allows from 0 to one under your upper range; can pass a lists length and will always get something in list"""
        return int((random.SystemRandom(random.seed()).random()) * upper_range)

    def __call__(self, func):

        print("Inside __call__()")
        def wrapped_func(*args):
            if self.queryset:
                for i in range(0, self.iterations):
                    random_index  = self.get_random(self.queryset_length)
                    random_object = self.queryset[random_index]
                    kwargs          = {'random_object': random_object}
                    func(*args, **kwargs)
            else:
                raise Exception('That callback returned no queryset results!')
        return wrapped_func

import requests
from urllib.parse import urlparse, parse_qs
from bs4 import BeautifulSoup

from ez_scrip_lib.sysadmin import write_content

def get_url_content(url):
    r = requests.get(url)
    return r.content

def get_soup(url):
    soup = BeautifulSoup(get_url_content(url), "html.parser")
    return soup

def write_soup_to_html(url, path='/tmp/soup.html'):
    soup = get_soup(url)
    write_content(path, soup.encode_contents())

def parse_query_params_from_url(full_url):
    parsed = urlparse(full_url) # full url like: https://cli.run.pivotal.io/stable?release=debian64&version=6.22.0&source=github-rel
    query_string = parsed.query
    query_params = parse_qs(query_string) # {'release': ['debian64'], 'source': ['github-rel'], 'version': ['6.22.0']}
    # now we have to unpack for some reason...
    for key, value in query_params.items():
        query_params[key] = value[0]
    return query_params

def get_all_links_on_page(url):
    soup = get_soup(url)
    links = [a_tag.attrs['href'] for a_tag in soup.find_all('a')]
    return links

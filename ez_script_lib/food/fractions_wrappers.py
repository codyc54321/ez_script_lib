from fractions import Fraction

def fraction_obj_to_string(fraction):
    if fraction.denominator == 1:
        return str(fraction.numerator)
    else:
        return str(fraction.numerator) + '/' + str(fraction.denominator)

def string_to_fraction_obj(fraction_string):
    n, d = fraction_string.split('/')
    return Fraction(int(n), int(d))
    
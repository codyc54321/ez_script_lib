from .fractions_wrappers import *

STANDARD_MEASUREMENTS       = {'tsp': 1, 'tbsp': 3, 'fluid_ounce': 6, 'cup': 48, 'pint': 96, 'quart': 192, 'gallon': 768}
TEASPOON_TO_ML              = 4.92892
ML_TO_LITER                 = 1000
    
def standard_to_metric(amount, denomination):
    return round((STANDARD_MEASUREMENTS[denomination] * amount * TEASPOON_TO_ML), 2)

def standard_to_standard(amount, start, finish):
    numerator   = STANDARD_MEASUREMENTS[start] * amount
    denominator = STANDARD_MEASUREMENTS[finish]
    return Fraction(numerator, denominator)
        
def metric_to_standard(milliliters, standard_denomination='tsp'):
    return round((milliliters / TEASPOON_TO_ML / STANDARD_MEASUREMENTS[standard_denomination]) , 2)


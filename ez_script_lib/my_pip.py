from .sysadmin import call_sp, get_project_path, search_for_file


def pip_install_in_venv(path):
    project = os.path.basename(os.path.normpath(path))
    call_sp("~/.virtualenvs/{}/bin/pip install -r requirements.txt".format(project), **{'cwd': path})


def needs_venv(path):
    return search_for_file('requirements.txt', path)
ACTIVE_BITBUCKET_PROJECTS = [
    'austin_eats', 'homebrew_app', 'recreate_other_homebrew_app', 'neo4j_sandbox', 'craigslist',
    'autohelper', 'bookwormbuddy'
]

ACTIVE_BITBUCKET_TUTORIALS= [
    'react-stormpath',
]


import re, difflib, filecmp

def content_the_same(a, b):
    diffler = difflib.SequenceMatcher(None, a, b)
    return diffler.ratio() == 1
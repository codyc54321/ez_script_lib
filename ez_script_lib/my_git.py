import os, sys, subprocess, json
try:
    import requests
except:
    requests = 'requests couldnt be imported...'

from .sysadmin import call_sp, ensure_dir_exists, get_project_path, ensure_home, PROJECT_PATH, HOMEPATH
from .general_and_string_processing import extract_values
# from .my_pip import needs_venvs ??? what happened here

from ez_scrip_lib.config import *


def is_git_repo(path):
    return search_for_dir('.git', path)

def update_project(path):
    project = os.path.basename(os.path.normpath(path))
    d = {'cwd': path}
    call_sp('git pull', **d)
    # if needs_venv(path):
    #     pip_install_in_venv(project, path)

def check_if_git_index_clean():
    try:
        x = subprocess.check_output(['git', 'diff', '--quiet'])
        return True
    except:
        return False

def remove_git_files(path):
    d = {'cwd': path}
    call_sp('rm -rf .git', **d)

def clone_project(project=None, dir_to_clone_in=HOMEPATH, username='codyc54321', hostname='bitbucket.org'):
    path = os.path.join(dir_to_clone_in, project)
    ensure_dir_exists(path)
    try:
        call_sp('git clone git@{hostname}:{username}/{project_name}.git {path}'.format(hostname=hostname,
                                                                                       username=username,
                                                                                       project_name=project,
                                                                                       path=path))
    except:
        print('\nThis project must already exist...\n\n')

def initial_push_project(username='codyc54321', project=None, hostname='bitbucket.org', relative_dir='projects'):
    project_path    = get_project_path(project_name=project, relative_dir=relative_dir)
    origin_command  = 'git remote set-url origin git@{hostname}:{username}/{project_name}.git'.format(hostname=hostname,
                                                                                                  username=username,
                                                                                                  project_name=project)
    d = {'cwd': project_path}
    call_sp(origin_command, **d)
    call_sp('git push -u origin --all', **d)

def push_all(username='codyc54321', project=None, hostname='bitbucket.org', relative_dir='projects'):
    """
    for each project dir in projects folder:
        add all files
        make a generic commit
        push it
    """


"""
https://confluence.atlassian.com/bitbucket/repository-resource-423626331.html

https://confluence.atlassian.com/bitbucket/use-the-bitbucket-cloud-rest-apis-222724129.html

http://docs.python-requests.org/en/v1.0.0/api/
"""

class BitbucketAPIHoss(object):

    API_URL_1 = 'https://api.bitbucket.org/1.0/'

    API_URL_2 = 'https://api.bitbucket.org/2.0/'

    HEADERS = {'Content-Type': 'application/json'}

    def __init__(self, username='codyc54321', password=APE_NINE):
        self.username = username
        self.password = password
        self.d = {'auth': (self.username, self.password), 'headers': self.HEADERS}

    def get_bitbucket_project_names(self):
        response = self.call_api('get', 'user/repositories', api_version="1")
        repo_dicts = self.convert_response_to_dict(response)
        repo_names = extract_values(key='name', dictionaries=repo_dicts)
        return repo_names

    def create_repository(self, name=None):
        payload = {'scm': 'git',
                   'name': name,
                   'is_private': 'false',

        }
        json_payload = json.dumps(payload)
        url_ending = os.path.join('repositories', self.username, name).lower()
        response = self.call_api('post', url_ending, data=json_payload)
        print(response.content)

    def delete_repository(self, name=None):
        url_ending = os.path.join('repositories', self.username, name).lower()
        response = self.call_api('delete', url_ending)
        print(response.content)

    def call_api(self, method, url_ending, api_version="2", **kwargs):
        if api_version == "2":
            api_url = self.API_URL_2
        elif api_version == "1":
            api_url = self.API_URL_1
        else:
            raise Exception("Pick API style 1 or 2")

        url = os.path.join(api_url, url_ending)
        print("\nCalling {}\n".format(url))
        print("Args: {}\n".format(str(kwargs)))
        request_call_function = getattr(requests, method)
        kwargs.update(self.d)
        return request_call_function(url, **kwargs)

    def convert_response_to_dict(self, response):
        return json.loads(response.content)
